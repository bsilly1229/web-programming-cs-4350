import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.css']
})
export class ResumeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public setHeightAndWidth(id: string):void{
    var item = document.getElementById(id);
    item.style.height = Math.max(item.scrollHeight,item.offsetHeight,item.clientHeight) + 4 + "px";
  }
}
