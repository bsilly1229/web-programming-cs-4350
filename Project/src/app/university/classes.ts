export interface ClassData{
    subject:string;
    course:string;
    section:string;
    credits:string;
    title:string;
    instructor:string;
    semester:string;
    grade:string;
}