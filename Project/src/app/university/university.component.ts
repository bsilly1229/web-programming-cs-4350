import { Component, OnInit } from '@angular/core';
import { ClassData } from './classes';

@Component({
  selector: 'app-university',
  templateUrl: './university.component.html',
  styleUrls: ['./university.component.css']
})

export class UniversityComponent implements OnInit {
  public classes:ClassData[];
  constructor() {
  }

  ngOnInit() {
    this.populateData();
  }

  public async populateData(){
    let url = "./assets/classes.json";
    this.classes = await this.loadData(url);
  }

  public async loadData(url: string): Promise<ClassData[]>{
    const response = await fetch(url);
    const json = await response.json();
    return json as ClassData[];
  }

}