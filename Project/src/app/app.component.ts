import { Component } from '@angular/core';
/*import{
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';*/

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
  /*animations:[
    routerLinkActive
  ]*/
})
export class AppComponent {
  title = 'Project';
}
