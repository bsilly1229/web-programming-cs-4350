import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResumeComponent } from './resume/resume.component';
import { TechnologiesUsedComponent } from './technologies-used/technologies-used.component';
import { GoalsComponent } from './goals/goals.component';
import { ExperienceComponent } from './experience/experience.component';
import { UniversityComponent } from './university/university.component';
import { AboutMeComponent } from './about-me/about-me.component';



const routes: Routes = [
  {path: 'resume', component: ResumeComponent},
  {path: 'technologies-used', component: TechnologiesUsedComponent},
  {path: 'goals', component: GoalsComponent},
  {path: 'experience', component: ExperienceComponent},
  {path: 'university', component: UniversityComponent},
  {path: '', component: AboutMeComponent},
  {path: 'about-me', component: AboutMeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})


export class AppRoutingModule { }
